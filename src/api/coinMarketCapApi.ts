import axios, { AxiosInstance } from "axios";
import * as dotenv from "dotenv";
import { QuoteResponse } from "../types/quotes";
import { ConversionResponse } from "../types/conversion";

dotenv.config();

export default class CoinMarket {
  baseURL: string = "https://pro-api.coinmarketcap.com/";
  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
      headers: { "X-CMC_PRO_API_KEY": process.env.CMC_PRO_API_KEY as string },
    });
  }

  async quotes(symbol: string[]) {
    const requestURL = `v1/cryptocurrency/quotes/latest?symbol=${symbol}`;

    try {
      const res = await this.axiosInstance.get(requestURL);

      const output: any = { data: {} };

      for (let i = 0; i < symbol.length; i++) {
        const currentSymbol = res.data.data[symbol[i]];

        const quoteInfo = {
          price: currentSymbol.quote.USD.price,
          last_updated: currentSymbol.quote.USD.last_updated,
        };

        const Quote = {
          USD: quoteInfo,
        };

        const symbolObject = {
          id: currentSymbol.id,
          name: currentSymbol.name,
          symbol: currentSymbol.symbol,
          slug: currentSymbol.slug,
          date_added: currentSymbol.date_added,
          last_updated: currentSymbol.last_updated,
          quote: Quote,
        };

        output.data[symbol[i]] = symbolObject;
      }

      return output as QuoteResponse;
    } catch (err) {
      if (axios.isAxiosError(err)) {
        return err.response?.data;
      }
    }
  }

  async conversion(symbol: string, amount: number, convert: string) {
    const requestURL = `v1/tools/price-conversion?amount=${amount}&symbol=${symbol}&convert=${convert}`;

    try {
      const res = await this.axiosInstance.get(requestURL);

      const output = res.data.data;

      return output as ConversionResponse;
    } catch (err) {
      if (axios.isAxiosError(err)) {
        return err.response?.data;
      }
    }
  }
}
