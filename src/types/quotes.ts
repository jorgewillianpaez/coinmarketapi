export interface QuoteResponse {
  data: Symbol;
}

export interface Symbol {
  [name: string]: CoinsInfo;
}

export interface CoinsInfo {
  id: number;
  name: string;
  symbol: string;
  data_added: Date;
  last_updated: Date;
  quote: Quote;
}

export interface Quote {
  USD: QuoteInfo;
}

export interface QuoteInfo {
  price: number;
  last_update: Date;
}
