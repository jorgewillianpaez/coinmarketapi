export interface ConversionResponse {
  data: ConversionSymbol;
}

export interface ConversionSymbol {
  [coin: string]: ConversionInfo;
}

export interface ConversionInfo {
  symbol: string;
  id: string;
  name: string;
  amount: number;
  last_updated: Date;
  quote: ConversionQuote;
}

export interface ConversionQuote {
  [CoinName: string]: ConversionQuoteInfo;
}

export interface ConversionQuoteInfo {
  price: number;
  last_updated: Date;
}
