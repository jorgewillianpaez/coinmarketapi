# Kencrypto-Lib

### Pacote que disponibiliza a consulta e conversão de moedas!

Primeiramente faça o clone do projeto com o seguinte comando:

```
git clone git@gitlab.com:jorgewillianpaez/coinmarketapi.git
```

Para adicionar o pacote, execute o seguinte comando no terminal do projeto:

```
npm install kencrypto-kenzie-lib
```
